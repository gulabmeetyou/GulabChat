import { Component, NgZone, ViewChild } from '@angular/core';
import { NavController, Content } from 'ionic-angular';
import { TextToSpeech } from '@ionic-native/text-to-speech';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  messages: any[] = [];
  text: String = "";
  @ViewChild(Content) content: Content;

  constructor(public navCtrl: NavController, public ngZone: NgZone, public tts: TextToSpeech) {

    this.messages.push({
      text: "Hi, Welcome to NUC CHAT! How can i help you?",
      sender: "robot"
    })

  }

  sendText()
  {
    let message = this.text;

    this.messages.push({
      text: message,
      sender: "me"
    });
    this.content.scrollToBottom(200);

    this.text="";

    window["ApiAIPlugin"].requestText({
      query: message
    }, (response)=>{

    this.ngZone.run(()=>{
      this.messages.push({
        text: response.result.fulfillment.speech,
        sender: "robot"
  });
  this.content.scrollToBottom(200);
})
    
    }, (error)=> {
      alert(JSON.stringify(error))
    })
  }

  sendVoice(){
    window["ApiAIPlugin"].requestVoice({},
    (response)=> {
      this.tts.speak({
        text: response.result.fulfillment.speech,
        locale: "en-IN",
        rate: 1
      })
      alert(response.result.fulfillment.speech)
    }, (error) => {
      alert(error)
    })
  
  
  }

}
